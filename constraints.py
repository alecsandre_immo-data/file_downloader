#années gardées pour les résultats et les cartes
currentYear = datetime.now().year
years = [currentYear, currentYear-1, currentYear-2, currentYear-3, currentYear-4, currentYear-5]
annee_scolaire = str(currentYear-1) + "-" + str(currentYear)

path = r'D:/immo-data/jeux_donnees/ecoles/recup/' #chemin du dossier pour enregistrer les DF

#url des DF
annuaire_url = r'https://data.education.gouv.fr/explore/dataset/fr-en-annuaire-education/download/?format=geojson&timezone=Europe/Berlin&lang=fr'
carte_colleges_url = r'https://data.education.gouv.fr/explore/dataset/fr-en-carte-scolaire-colleges-publics/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B'
carte_colleges_paris_url = r'https://opendata.paris.fr/explore/dataset/secteurs-scolaires-colleges/download/?format=geojson&timezone=Europe/Berlin&lang=fr'
carte_maternelles_paris_url = r'https://opendata.paris.fr/explore/dataset/secteurs-scolaires-maternelles/download/?format=geojson&timezone=Europe/Berlin&lang=fr'
carte_elementaires_paris_url = r'https://opendata.paris.fr/explore/dataset/secteurs-scolaires-ecoles-elementaires/download/?format=geojson&timezone=Europe/Berlin&lang=fr'
brevet_url = r'https://data.education.gouv.fr/explore/dataset/fr-en-dnb-par-etablissement/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B'
bac_pro_url = r'https://data.education.gouv.fr/explore/dataset/fr-en-indicateurs-de-resultat-des-lycees-denseignement-professionnels/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B'
bac_gen_url = r'https://data.education.gouv.fr/explore/dataset/fr-en-indicateurs-de-resultat-des-lycees-denseignement-general-et-technologique/download/?format=csv&timezone=Europe/Berlin&lang=fr&use_labels_for_header=true&csv_separator=%3B'

bacLast_rename = ({"UAI":"ID",
                  "Taux de reussite - Toutes series":"successRateAll",
                  "Taux de mentions - Toutes series":"mentionRateAll",
                  "Valeur ajoutee du taux de reussite - Toutes series":"addedValueAll",
                  "Taux de reussite - Gnle":"successRateGnle",
                  "Valeur ajoutee du taux de réussite - Gnle":"addedValueGnle",
                  "Taux de mentions - Gnle":"mentionRateGnle",
                  "taux_reussite_moyen_toutes_series":"MeanSuccessRateAll",
                  "taux_reussite_moyen_Gnle":"MeanSuccessRateGnle",
                  "taux_mentions_moyen_toutes_series":"MeanMentionRateAll",
                  "taux_mentions_moyen_Gnle":"MeanMentionRateGnle",
                  "valeur_ajoutee_moyenne_toutes_series":"MeanAddedValueAll",
                  "valeur_ajoutee_moyenne_gnle":"MeanAddedValueGnle"})
                  
lyceeTauxReussiteOk = ["Annee","UAI",
                     "Taux de reussite - Toutes series",
                     "Taux de mentions - Toutes series",
                     "Valeur ajoutee du taux de reussite - Toutes series",
                     "Taux de reussite - Gnle",
                     "Valeur ajoutee du taux de réussite - Gnle",
                     "Taux de mentions - Gnle"]
                  
brevetLast_rename = {"Numero d'etablissement":"ID",
                  "Taux de réussite":"brevetLastSuccessRate",
                    "taux_reussite_moyen":"brevetMeanSuccessRate"}

df_annuaire_rename = {"nom_etablissement":"name",
                     "code_commune":"inseeCode",
                     "statut_public_prive":"publicPrivate",
                     "identifiant_de_l_etablissement":"ID",
                     "nom_commune":"city",
                     "libelle_nature":"detailedType",
                     "etat":"openState"}
                     
ecoles_replace = {"COLLEGE":"COLLEGE",
                  "COLLEGE SPECIALISE":"COLLEGE",
                  "COLLEGE CLIMATIQUE":"COLLEGE",
                  "ECOLE MATERNELLE D APPLICATION":"ECOLE MATERNELLE",
                  "ECOLE ELEMENTAIRE D APPLICATION":"ECOLE DE NIVEAU ELEMENTAIRE",
                  "LYCEE ENSEIGNT GENERAL ET TECHNOLOGIQUE":"LYCEE",
                  "LYCEE D ENSEIGNEMENT GENERAL":"LYCEE",
                  "LYCEE POLYVALENT":"LYCEE",
                  "SECTION ENSEIGT GENERAL ET TECHNOLOGIQUE":"LYCEE",
                  "LYCEE EXPERIMENTAL":"LYCEE",
                  "LYCEE D ENSEIGNEMENT TECHNOLOGIQUE":"LYCEE_AUTRE",
                  "LYCEE ENS GENERAL TECHNO PROF AGRICOLE":"LYCEE_AUTRE",
                  "LYCEE PROFESSIONNEL":"LYCEE_AUTRE",
                  "SECTION D ENSEIGNEMENT PROFESSIONNEL":"LYCEE_AUTRE",
                  "ECOLE REGIONALE DU PREMIER DEGRE":"ECOLE_SPECIALE",
                  "ECOLE DE PLEIN AIR":"ECOLE_SPECIALE",
                  "ECOLE PROFESSIONNELLE SPECIALISEE":"ECOLE_SPECIALE",
                  "CETAD (TOM)":"ECOLE_SPECIALE",
                  "ECOLE MATERNELLE ANNEXE D INSPE":"ECOLE_SPECIALE",
                  "ECOLE DE NIVEAU ELEMENTAIRE SPECIALISEE":"ECOLE_SPECIALE",
                  "ECOLE SANS EFFECTIFS PERMANENTS":"ECOLE_SPECIALE",
                  "LYCEE CLIMATIQUE":"ECOLE_SPECIALE",
                  "ETAB REGIONAL/LYCEE ENSEIGNEMENT ADAPTE":"ECOLE_SPECIALE",
                  "ETABLISSEMENT DE REINSERTION SCOLAIRE":"ECOLE_SPECIALE",
                  "ECOLE SECONDAIRE SPECIALISEE (2 D CYCLE)":"ECOLE_SPECIALE",
                 }
                 
df_carte_rename = {"code_insee":"inseeCode",
                  "libelle_commune":"city",
                  "Type_et_Libelle":"streetName",
                  "Code_RNE":"ID"}
                  
df_carte_paris_rename = {"type_etabl":"schoolType",
                     "libelle":"name"}
                     

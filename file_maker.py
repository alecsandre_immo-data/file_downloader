import pandas as pd
import numpy as np
import geopandas as gpd
from shapely.geometry import Polygon, LineString, Point, box
from shapely import geometry
from math import cos, sin, asin, sqrt, radians
from ast import literal_eval
import math
from datetime import datetime
import requests
import os
from constraints import *

#dataframes de cartes scolaires de paris
college = gpd.read_file(carte_colleges_paris_url, crs="EPSG:2154")
college_drop = ["annee_scol","zone_commune","id_projet","etiquette","lib_etab_1","lib_etab_2"]
college = college[college["type_etabl"].notna()]
college = college[college["annee_scol"] == annee_scolaire]
college = college.drop(college_drop, axis = 1)

maternelle = gpd.read_file(carte_maternelles_paris_url, crs="EPSG:2154")
mat_drop = ["annee_scol","zone_commune","id_projet","etiquette","lib_etab_1","lib_etab_2","lib_etab_3","lib_etab_4"]
maternelle = maternelle[maternelle["type_etabl"].notna()]
maternelle = maternelle[maternelle["annee_scol"] == annee_scolaire]
maternelle = maternelle.drop(mat_drop, axis = 1)

elementaire = gpd.read_file(carte_elementaires_paris_url, crs="EPSG:2154")
elem_drop = ["annee_scol","zone_commune","id_projet","etiquette","lib_etab_1","lib_etab_2","lib_etab_3","lib_etab_4"]
elementaire = elementaire[elementaire["type_etabl"].notna()]
elementaire = elementaire[elementaire["annee_scol"] == annee_scolaire]
elementaire = elementaire.drop(elem_drop, axis = 1)

frames = [elementaire,college,maternelle]
df_carte_paris = pd.concat(frames)
df_carte_paris = df_carte_paris.rename(df_carte_paris_rename, axis = 1)


#carte scolaire des colleges de france (avec certaines données manquantes)
df_carte = pd.read_csv(carte_colleges_url, sep=";")
df_carte_keep = ["code_insee","libelle_commune","Type_et_Libelle","lieu_dit","No_de_voie_debut","Indice_de_repetition_debut","No_de_voie_fin","Indice_de_repetition_fin","Code_RNE"]
df_carte = df_carte[df_carte_keep]
df_carte["code_insee"] = df_carte["code_insee"].astype(str)
df_carte["code_insee"] = df_carte["code_insee"].str.zfill(5)
df_carte = df_carte.rename(df_carte_rename, axis = 1)


#annuaire des écoles en france
df_annuaire = gpd.read_file(annuaire_url)
df_annuaire_keep = ['nom_etablissement','code_commune','statut_public_prive','identifiant_de_l_etablissement',
                     'adresse_1', 'longitude','adresse_2','adresse_3','latitude','nom_commune','libelle_nature','geometry','etat']
df_annuaire = df_annuaire[df_annuaire_keep]
df_annuaire = gpd.GeoDataFrame(df_annuaire, geometry=gpd.points_from_xy(df_annuaire.latitude, df_annuaire.longitude))
df_annuaire["type"] = df_annuaire.libelle_nature.replace(ecoles_replace)
df_annuaire = df_annuaire.rename(df_annuaire_rename, axis = 1)


#résultats brevet 
brevet = pd.read_csv(brevet_url, sep = ";")
brevet["Taux de réussite"] = brevet["Taux de réussite"].str.replace("%","").str.replace(",",".").astype(float)
brevet = brevet[["Session","Numero d'etablissement","Admis","Admis sans mention","Taux de réussite"]] 

brevet = brevet[brevet['Session'].isin(years)]
brevetLast = brevet[brevet['Session'] == brevet['Session'].max()]

moyenne_etablissement = brevet.groupby("Numero d'etablissement")['Taux de réussite'].mean().to_frame().reset_index()
moyenne_etablissement = moyenne_etablissement.rename({"Taux de réussite": "taux_reussite_moyen"}, axis = 1)

brevetLast = pd.merge(brevetLast,moyenne_etablissement, on = "Numero d'etablissement", how = 'left').drop("Session",axis = 1)
brevetLast["brevetMentionRate"] = 100 - ((brevetLast["Admis sans mention"] * 100)/brevetLast["Admis"])
brevetLast['test'] = ((brevetLast["Admis sans mention"]*100) / brevetLast["Admis"]) 
brevetLast = brevetLast.rename(brevetLast_rename, axis = 1)


#résultat bac
bac_pro = pd.read_csv(r'D:\immo-data\jeux_donnees\ecoles\fr-en-indicateurs-de-resultat-des-lycees-denseignement-professionnels.csv', sep = ";")
bac_gen_tech = pd.read_csv(r'D:\immo-data\jeux_donnees\ecoles\fr-en-indicateurs-de-resultat-des-lycees-denseignement-general-et-technologique.csv', sep = ";")
bacTotal = pd.concat([bac_gen_tech,bac_pro])
bacTotal = bacTotal[lyceeTauxReussiteOk]

bacTotal['Valeur ajoutee du taux de reussite - Toutes series'] = bacTotal['Valeur ajoutee du taux de reussite - Toutes series'].replace("ND", np.nan).replace(".", np.nan)
bacTotal['Valeur ajoutee du taux de reussite - Toutes series'] = bacTotal['Valeur ajoutee du taux de reussite - Toutes series'].astype(float)
bacTotal['Valeur ajoutee du taux de réussite - Gnle'] = bacTotal['Valeur ajoutee du taux de réussite - Gnle'].replace("ND", np.nan).replace(".", np.nan)
bacTotal['Valeur ajoutee du taux de réussite - Gnle'] = bacTotal['Valeur ajoutee du taux de réussite - Gnle'].astype(float)

bac = bacTotal[bacTotal['Annee'].isin(years)]
bacLast = bacTotal[bacTotal['Annee'] == brevet['Session'].max()]

moyenne_series = bac.groupby("UAI")['Taux de reussite - Toutes series'].mean().to_frame().reset_index()
moyenne_series = moyenne_series.rename({"Taux de reussite - Toutes series": "taux_reussite_moyen_toutes_series"}, axis = 1)
bacLast = pd.merge(bacLast,moyenne_series, on = "UAI", how = 'left').drop("Annee",axis = 1)

moyenne_gnle = bac.groupby("UAI")['Taux de reussite - Gnle'].mean().to_frame().reset_index()
moyenne_gnle = moyenne_gnle.rename({"Taux de reussite - Gnle": "taux_reussite_moyen_Gnle"}, axis = 1)
bacLast = pd.merge(bacLast,moyenne_gnle, on = "UAI", how = 'left')

moyenne_mentions_series = bac.groupby("UAI")['Taux de mentions - Toutes series'].mean().to_frame().reset_index()
moyenne_mentions_series = moyenne_mentions_series.rename({"Taux de mentions - Toutes series": "taux_mentions_moyen_toutes_series"}, axis = 1)
bacLast = pd.merge(bacLast,moyenne_mentions_series, on = "UAI", how = 'left')

moyenne_mentions_gnle = bac.groupby("UAI")['Taux de mentions - Gnle'].mean().to_frame().reset_index()
moyenne_mentions_gnle = moyenne_mentions_gnle.rename({"Taux de mentions - Gnle": "taux_mentions_moyen_Gnle"}, axis = 1)
bacLast = pd.merge(bacLast,moyenne_mentions_gnle, on = "UAI", how = 'left')

moyenne_Valeurajoutee_series = bac.groupby("UAI")['Valeur ajoutee du taux de reussite - Toutes series'].mean().to_frame().reset_index()
moyenne_Valeurajoutee_series = moyenne_Valeurajoutee_series.rename({"Valeur ajoutee du taux de reussite - Toutes series": "valeur_ajoutee_moyenne_toutes_series"}, axis = 1)
bacLast = pd.merge(bacLast,moyenne_Valeurajoutee_series, on = "UAI", how = 'left')

moyenne_Valeurajoutee_gnle = bac.groupby("UAI")['Valeur ajoutee du taux de réussite - Gnle'].mean().to_frame().reset_index()
moyenne_Valeurajoutee_gnle = moyenne_Valeurajoutee_gnle.rename({'Valeur ajoutee du taux de réussite - Gnle': "valeur_ajoutee_moyenne_gnle"}, axis = 1)
bacLast = pd.merge(bacLast,moyenne_Valeurajoutee_gnle, on = "UAI", how = 'left')
bacLast = bacLast.rename(bacLast_rename,axis = 1)


#creation fichier
bacLast.to_csv(path + r'bac.csv', index = False, sep = ";")
brevetLast.to_csv(path + r'brevet.csv', index = False, sep = ";")
df_carte.to_csv(path + r'carte_colleges.csv', index = False, sep = ";")
df_carte_paris.to_file(path + r"carte_paris.geojson", driver='GeoJSON')
df_annuaire.to_file(path + r"annuaire.geojson", driver='GeoJSON')